package com.citi.training.employees.dao;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.model.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlEmployeeDaoTests {

    @Autowired
    MysqlEmployeeDao mysqlEmployeeDao;

    @Test
    @Transactional
    public void test_createAndFindAll() {
        mysqlEmployeeDao.create(new Employee(-1, "Frank", 10.0));

        assert(mysqlEmployeeDao.findAll().size() >= 1);
    }

}
